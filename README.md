# README #

Suit of libraries and executables that implement and explore crypto algorithms.

# How to build #
## Prereqs ##
This project is CMAKE based. As such, you need CMAKE (min version 2.8).

Additionally, most of the code uses C++14 so you need a compiler with that featurset.

The latest GCC, CLANG will work fine.

## Steps ##
1. Start in Project root
2. `mkdir build && cd build`
3. `cmake ..`
4. `make`
5. (optional) `make test`
6. Binaries and static libraries are in the bin/ directory.

# Repo Structure #
bin/ -- contains prebuilt static libs and executables (compiled for 64-bit linux; Fedora 27)


components/ -- contains subproject folders for various things

components/aes -- static library implementation for aes (AesLib)

components/lame\_aes -- Executable frontend for AesLib


catch/ -- local copy of Catch unit testing header


final\_project\_test\_cases/ -- contains results and test cases for the final project deliverables

final\_project\_test\_cases/results -- contains the results of the final project deliberables


# AES Implementation lame\_aes ##

Implementation using C++14 and the STL. 

Test cases are created using [Catch](https://github.com/catchorg/Catch2).

Command Line Parsing done with [Args](https://github.com/Taywee/args).


Everything else is hand-rolled.

## Usage ##
Copy of the -h switch:
> bin/lame_aes [data] [key] {OPTIONS}
>
>       lame_aes is a lame implementation of aes
> 
>       OPTIONS:
>
>            -h, --help                        Display this help menu
>          Arguments
>            -d, --decrypt                     Decrypt rather than encrypt
>            -c, --cbc                         Use CBC encryption rather than ECB
>            -f, --file                        Use file names not raw strings
>          data                              Raw data as hex string or data file name
>          key                               Raw key as hex string or key file name
>          "--" can be used to terminate flag options and force all following
>          arguments to be treated as positional options
>
>       Author: Steven Frederiksen
>       For MATH 4175, Prof. Heath David Hart

## Examples ##
`bin/lame_aes 45b7cf11839538da7da1ca40c3f4b924a3f6aca53d3d496f4 f6d2738e2589cb88487aa5e49834f46e79550cb2fa393c80ccb67f93f93b5c3c`

`bin/lame_aes plaintext.txt key.txt -f`

`bin/lame_aes plaintext.txt key.txt -f -c`

`bin/lame_aes ciphertext.txt key.txt -c -d -f`

# Final Project Deliverables ##

final\_project\_test\_cases/ contains the test files given. It also contains a python3 script to count the number of 1's in the XOR between two given hex strings `count1s.py`. It converts them to ints, XORs, then counts the ones.

final\_project\_test\_cases/results contains the final project deliverables:

### Deliverable 1 ###
aes-ciphertext11-ecb.txt

aes-ciphertext11-cbc.txt

### Deliverable 2 ###
aes-ciphertext12-ecb.txt

aes-ciphertext12-cbc.txt

### Deliverable 3 ###
aes-ciphertext13-ecb.txt

aes-ciphertext13-cbc.txt

### Deliverable 4 ###
aes-plaintext10.txt

10.png -- plaintext converted into binary with a png extension; it is indeed a black box

### Deliverable 5 ###
Baseline values

aes\_secrecy\_baseline-plaintext.txt

aes\_secrecy\_baseline-key.txt

aes\_secrecy\_baseline-ciphertext.txt


### Deliverables 6,7,8,9 ###
Outputs on 1 counts from the `count1s.py` script.

aes\_secrecy\_6-count1s.txt

aes\_secrecy\_7-count1s.txt

aes\_secrecy\_8-count1s.txt

aes\_secrecy\_9-count1s.txt
