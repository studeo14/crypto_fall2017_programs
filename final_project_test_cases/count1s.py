#!/usr/bin/python3
import sys
if len(sys.argv) != 3:
    exit(1)
else:
    #get two input hex strings
    a = int(sys.argv[1],16)
    b = int(sys.argv[2],16)
    #XOR and turn into bitstring
    c = str(bin(a^b))
    #count total bits
    c_l = len(c)-2 #remove 0b from front
    #count 1's
    c_n1 = c.count('1')
    #report ratio
    print('Percent Different: {}%'.format((c_n1/c_l) * 100))
