//
// Created by sfrederiksen on 9/14/17.
//

#include <iostream>
#include <string>
#include <exception>
#include <fstream>
#include <sstream>
#include "aes.h"
#include "args.hxx"

void read_files( aes_key_t &data, aes_key_t &key, const std::string &dataName, const std::string &keyName );

void convert_to_stream( aes_key_t &data, aes_key_t &key, const std::string &dataS, const std::string &keyS );

int main( int argc, char **argv ) {
    //command line arg parser
    args::ArgumentParser parser( "lame_aes is a lame implementation of aes", "Author: Steven Frederiksen\nFor MATH 4175, Prof. Heath David Hart" );
    //help
    args::HelpFlag help( parser, "help", "Display this help menu", { 'h', "help" } );
    //options
    args::Group options( parser, "Arguments", args::Group::Validators::DontCare );
    //use decryption
    args::Flag dec( options, "decryption", "Decrypt rather than encrypt", { 'd', "decrypt" } );
    //use cbc
    args::Flag cbc( options, "cbc", "Use CBC encryption rather than ECB", { 'c', "cbc" } );
    //use file names
    args::Flag file( options, "file", "Use file names not raw strings", { 'f', "file" } );
    //raw string inputs or file names
    args::Positional< std::string > data( parser, "data", "Raw data as hex string or data file name" );
    args::Positional< std::string > key( parser, "key", "Raw key as hex string or key file name" );
    //parse
    try {
        parser.ParseCLI( argc, argv );
    } catch ( args::Help & ) {
        std::cout << parser;
        return 0;
    }
    catch ( args::ParseError &e ) {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return 1;
    }
    catch ( args::ValidationError &e ) {
        std::cerr << e.what() << std::endl;
        std::cerr << parser;
        return 1;
    }

    //now do stuff
    aes_key_t rawData, rawKey;
    aes_key_ptr_t output;

    try {
        if ( file ) {
            //open key file and read in as hex string
            read_files( rawData, rawKey, args::get( data ), args::get( key ));
        } else {
            convert_to_stream( rawData, rawKey, args::get( data ), args::get( key ));
        }

        if ( dec ) {
            if ( cbc ) {
                aes::decryptors::decrypt_cbc aes( rawKey );
                output = aes.decrypt_stream( rawData );
            } else {
                aes::decryptors::decrypt_ecb aes( rawKey );
                output = aes.decrypt_stream( rawData );
            }
        } else {
            if ( cbc ) {
                aes::encryptors::encrypt_cbc aes( rawKey );
                output = aes.encrypt_stream( rawData );
            } else {
                aes::encryptors::encrypt_ecb aes( rawKey );
                output = aes.encrypt_stream( rawData );
            }
        }
    } catch ( std::invalid_argument &e ) {
        std::cerr << e.what() << std::endl;
        return 1;
    }

    aes::util::println( *output, true );

    return 0;
}

char char2int( char input ) {
    if ( input >= '0' && input <= '9' )
        return input - '0';
    if ( input >= 'A' && input <= 'F' )
        return input - 'A' + 10;
    if ( input >= 'a' && input <= 'f' )
        return input - 'a' + 10;
    throw std::invalid_argument( "Invalid input string" );
}

void read_files( aes_key_t &data, aes_key_t &key, const std::string &dataName, const std::string &keyName ) {
    //open files
    std::ifstream dataFile( dataName ), keyFile( keyName );

    if ( !dataFile.is_open() || !keyFile.is_open()) {
        throw std::invalid_argument( "Files could not be opened" );
    }
    //else
    char tmp1, tmp2;
    while ( dataFile >> tmp1 >> tmp2 ) {
        tmp1 = char2int(tmp1);
        tmp2 = char2int(tmp2);
        data.push_back( (tmp1 << 4) | tmp2 );
    }
    while ( keyFile >> tmp1 >> tmp2 ) {
        tmp1 = char2int(tmp1);
        tmp2 = char2int(tmp2);
        key.push_back( (tmp1 << 4) | tmp2 );
    }
}

void convert_to_stream( aes_key_t &data, aes_key_t &key, const std::string &dataS, const std::string &keyS ) {
    std::stringstream dataSS(dataS), keySS(keyS);
    char tmp1, tmp2;
    while ( dataSS >> tmp1 >> tmp2 ) {
        tmp1 = char2int(tmp1);
        tmp2 = char2int(tmp2);
        data.push_back( (tmp1 << 4) | tmp2 );
    }
    while ( keySS >> tmp1 >> tmp2 ) {
        tmp1 = char2int(tmp1);
        tmp2 = char2int(tmp2);
        key.push_back( (tmp1 << 4) | tmp2 );
    }
}
