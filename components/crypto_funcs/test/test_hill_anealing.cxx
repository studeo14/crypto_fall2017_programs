//
// Created by sfrederiksen on 9/2/17.
//

#include "catch.hpp"
#include "crypto.h"
#include <algorithm>
#include <vector>


TEST_CASE( "Message is found from shift encrypted cryptext", "[HA]" ) {
    //needs to be clean
    std::vector< std::string > texts = {
            "tobeornottobethatisthequestion",
            "upcfpsopuupcfuibujtuifrvftujpo",
            "vqdgqtpqvvqdgvjcvkuvjgswguvkqp",
            "wrehruqrwwrehwkdwlvwkhtxhvwlrq",
            "xsfisvrsxxsfixlexmwxliuyiwxmsr",
            "ytgjtwstyytgjymfynxymjvzjxynts",
            "zuhkuxtuzzuhkzngzoyznkwakyzout",
            "avilvyuvaavilaohapzaolxblzapvu",
            "bwjmwzvwbbwjmbpibqabpmycmabqwv",
            "cxknxawxccxkncqjcrbcqnzdnbcrxw",
            "dyloybxyddylodrkdscdroaeocdsyx",
            "ezmpzcyzeezmpesletdespbfpdetzy",
            "fanqadzaffanqftmfueftqcgqefuaz",
            "gborbeabggborgungvfgurdhrfgvba",
            "hcpscfbchhcpshvohwghvseisghwcb",
            "idqtdgcdiidqtiwpixhiwtfjthixdc",
            "jeruehdejjerujxqjyijxugkuijyed",
            "kfsvfiefkkfsvkyrkzjkyvhlvjkzfe",
            "lgtwgjfgllgtwlzslaklzwimwklagf",
            "mhuxhkghmmhuxmatmblmaxjnxlmbhg",
            "nivyilhinnivynbuncmnbykoymncih",
            "ojwzjmijoojwzocvodnoczlpznodji",
            "pkxaknjkppkxapdwpeopdamqaopekj",
            "qlybloklqqlybqexqfpqebnrbpqflk",
            "rmzcmplmrrmzcrfyrgqrfcoscqrgml",
            "snadnqmnssnadsgzshrsgdptdrshnm",
    };
    std::string message = "tobeornottobethatisthequestion";
    SECTION( "Testing all possible keys will work" ) {
        std::for_each( texts.begin(), texts.end(), [&message]( auto str ) {
            REQUIRE(
                    crypto::string_ana::hill_with_anealing( str, false )
                        ==
                    message
            );
        } );
    }
}

SCENARIO( "Message with known key is decrypted", "[shift]" ) {
    GIVEN( "some test" ) {
        //needs to be clean
        std::string cryptext = "bwjmwzvwbbwjmbpibqabpmycmabqwv";
        std::string message = "tobeornottobethatisthequestion";
        WHEN( "analyzed" ) {
            auto HA = crypto::string_ana::shift_decrypt( cryptext, 8 );
            THEN( "The message is correct" ) {
                REQUIRE( message == HA );
            }
        }

    }
}