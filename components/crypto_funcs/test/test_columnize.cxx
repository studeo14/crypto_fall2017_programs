//
// Created by sfrederiksen on 9/2/17.
//

#include "catch.hpp"
#include "crypto.h"


SCENARIO( "Columnize is tested", "[columnize]" ) {
    GIVEN( "a string of chars with a rectangular dims" ) {
        std::string chars = "aaaaahhhhhlllllkkkkkiiiiiaaaaahhhhhlllllkkkkkiiiiiaaaaahhhhhlllllkkkkkiiiiiaaaaahhhhhlllllkkkkkiiiiiaaaaahhhhhlllllkkkkkiiiiiaaaaahhhhhlllllkkkkkiiiiiaaaaahhhhhlllllkkkkkiiiiiaaaaahhhhhlllllkkkkkiiiii";
        WHEN( "analyzed" ) {
            auto freq_count = crypto::util::columnize( chars, 5 );
            THEN("The frequencies are correct"){
                REQUIRE( crypto::util::decolumnize(freq_count) == chars );
            }
        }
    }
}