/**
 * Author:      Steven Frederiksen
 * Purpose:     Perform a test for breaking a permutation cypher
 */
#include "catch.hpp"
#include "crypto.h"
#include <iostream>

SCENARIO( "Permutation cypher cryptotext can be broken", "[permutation]" ) {
    GIVEN( "Some cryptotext, it's size, and a known working factor pair" ) {
        std::string
                cryptext( "ctaropyghpry" ),
                message( "" );
        std::pair< int, int > factors = std::make_pair( 4, 3 );
        WHEN( "processed " ) {
            message = crypto::string_ana::find_message( factors, cryptext.size(), cryptext );
            THEN( "the message is broken" ) {
                REQUIRE( message == "cryptography" );
            }
        }
    }
}

SCENARIO("Permutation Cypher Breaking Program", "[permutation_break]"){
    GIVEN( "Some cryptotext only" ) {
        std::string
                cryptext( "ctaropyghpry" ),
                message( "" );
        WHEN( "processed " ) {
            message = crypto::string_ana::break_perm( cryptext );
            THEN( "the message is broken" ) {
                REQUIRE( message == "cryptography" );
            }
        }
    }
}
