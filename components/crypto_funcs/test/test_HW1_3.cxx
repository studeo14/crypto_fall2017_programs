//
// Created by sfrederiksen on 9/2/17.
//

#include "catch.hpp"
#include "crypto.h"
#include <iostream>


TEST_CASE( "Count Letter", "[frequency_counter]" ) {
    //prompt
    std::cout << "Enter string to count: ";
    std::string message;
    std::getline(std::cin, message);
    std::cout << "Results: ";
    crypto::string_ana::print_freq_list( *( crypto::string_ana::frequency_search( message )));
}