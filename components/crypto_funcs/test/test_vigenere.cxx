//
// Created by sfrederiksen on 9/2/17.
//

#include <iostream>
#include "catch.hpp"
#include "crypto.h"


SCENARIO( "Vigenere break is tested", "[vigenere]" ) {
    GIVEN( "a string of chars with a rectangular dims" ) {
        std::string chars = "NGMNITSKCXIPOESDSKKXGMEJVC";
        std::string message = "dcodevigenereautomatically";
        WHEN( "analyzed" ) {
            auto vigenere_text = crypto::string_ana::break_vigenere( chars, false );
            THEN("The frequencies are correct"){
                REQUIRE( message == vigenere_text );
            }
        }
    }
}