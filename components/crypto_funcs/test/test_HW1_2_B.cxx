//
// Created by sfrederiksen on 9/2/17.
//

#include "catch.hpp"
#include "crypto.h"
#include <string>
#include <iostream>

TEST_CASE( "Generic Breaking Test", "[perm_break_test]" ) {
    //get a cryptext
    std::cout << "Enter cryptext: ";
    std::string ctext;
    std::cin >> ctext;
    //try to break
    crypto::string_ana::break_perm( ctext );
}