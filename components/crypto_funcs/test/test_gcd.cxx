//
// Created by sfrederiksen on 9/4/17.
//

#include <iostream>
#include "catch.hpp"
#include "crypto.h"

SCENARIO("GCDs are calculated.","[gcd]"){
    GIVEN("Pairs of integers"){
        std::pair<int32_t,int32_t>
            four(36,988),    // 4
            two(74,654),    // 2
            three(12,3),    // 3
            one(14478,923) // 1
        ;
        WHEN("Their GCDs are calculated"){
            int32_t
                four_res = crypto::math::gcd(four),
                three_res = crypto::math::gcd(three),
                two_res = crypto::math::gcd(two),
                one_res = crypto::math::gcd(one)
            ;
            THEN("The results are correct"){
                REQUIRE(four_res == 4);
                REQUIRE(three_res == 3);
                REQUIRE(two_res == 2);
                REQUIRE(one_res == 1);
            }
        }
    }
}

SCENARIO("Extended GCDs are calculated", "[gcd_extended]"){
    GIVEN("Pairs of integers"){
        std::pair<int32_t,int32_t>
            four(36,988),    // 4
            two(74,654),    // 2
            three(12,3),    // 3
            one(14478,923) // 1
        ;
        WHEN("Their GCDs are calculated"){
            int32_t
                x4,y4,
                x3,y3,
                x2,y2,
                x1,y1
            ;
            int32_t
                four_res = crypto::math::gcd_extended(four, x4, y4),
                three_res = crypto::math::gcd_extended(three, x3, y3),
                two_res = crypto::math::gcd_extended(two, x2, y2),
                one_res = crypto::math::gcd_extended(one, x1, y1)
            ;
            THEN("The results are correct"){
                std::cout
                    << x1 << std::endl
                    << x2 << std::endl
                    << x3 << std::endl
                    << x4 << std::endl
                ;
                REQUIRE(four_res == 4);
                REQUIRE(three_res == 3);
                REQUIRE(two_res == 2);
                REQUIRE(one_res == 1);
            }
        }

    }
}