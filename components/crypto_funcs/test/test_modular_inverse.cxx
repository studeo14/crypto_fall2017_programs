//
// Created by sfrederiksen on 9/4/17.
//

#include <iostream>
#include "catch.hpp"
#include "crypto.h"

SCENARIO( "Modular inverses are calculated.", "[modular_inverse]" ) {
    GIVEN( "some values and mods" ) {
        int32_t
                a1( 7 ), m1( 26 ),  // 15
                a2( 19 ), m2( 89 ), // 75
                a3( -19 ), m3( 89 ),// 14
                a4( 653 ), m4( 1287719 ) // 857822
        ;
        WHEN( "Their GCDs are calculated" ) {
            int32_t
                    three_res = crypto::math::modular_inverse( a3, m3 ),
                    two_res = crypto::math::modular_inverse( a2, m2 ),
                    four_res = crypto::math::modular_inverse( a4, m4 ),
                    one_res = crypto::math::modular_inverse( a1, m1 );
            THEN( "The results are correct" ) {
                REQUIRE( four_res == 857822 );
                REQUIRE( three_res == 14 );
                REQUIRE( two_res == 75 );
                REQUIRE( one_res == 15 );
            }
        }
    }
}

TEST_CASE( "Modular Inverse Test", "[MI_test]" ) {
    //get the inputs
    int32_t a, m;
    std::cout << "A : ";
    std::cin >> a;
    std::cout << "M : ";
    std::cin >> m;
    int32_t MI = crypto::math::modular_inverse( a, m );
    if ( MI == -1 ) {
        std::cout << a << " is not invertible under " << m << std::endl;
    } else {
        std::cout << "MI = " << MI << std::endl;
    }
}

