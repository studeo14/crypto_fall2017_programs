//
// Created by sfrederiksen on 9/2/17.
//

#include "catch.hpp"
#include "crypto.h"


SCENARIO( "Frequency of letters is counted", "[frequency_search]" ) {
    GIVEN( "a string of chars" ) {
        std::string chars = "aaaaaffffggghhjji";
        WHEN( "analyzed" ) {
            auto freq_count = crypto::string_ana::frequency_search( chars );
            THEN("The frequencies are correct"){
                REQUIRE( crypto::util::get_value<uint32_t>(*freq_count, 'a') == 5 );
                REQUIRE( crypto::util::get_value<uint32_t>(*freq_count, 'f') == 4 );
                REQUIRE( crypto::util::get_value<uint32_t>(*freq_count, 'g') == 3 );
                REQUIRE( crypto::util::get_value<uint32_t>(*freq_count, 'h') == 2 );
                REQUIRE( crypto::util::get_value<uint32_t>(*freq_count, 'j') == 2 );
                REQUIRE( crypto::util::get_value<uint32_t>(*freq_count, 'i') == 1 );
            }
        }
    }
}