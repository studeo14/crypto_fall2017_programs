//
// Created by sfrederiksen on 9/2/17.
//

#include "catch.hpp"
#include "crypto.h"
#include <sstream>

SCENARIO( "LFSR Encryption", "[LFSR]" ) {
    GIVEN( "a plaintext stream of 1's and 0's" ) {
        std::string message = "00100100001111";
        std::string ciphert = "11001101111011";
        std::string seed = "1110";
        WHEN( "encrypted" ) {
            std::ostringstream out;
            std::istringstream in( message );
            crypto::lfsr::digest( out, in, seed );
            THEN( "The ciphertext is correct" ) {
                REQUIRE( out.str() == ciphert );
            }
        }
    }
}