//
// Created by sfrederiksen on 9/2/17.
//

#include "catch.hpp"
#include "crypto.h"
#include <iostream>
#include <limits>


SCENARIO( "Log Quad score is found", "[LQ]" ) {
    GIVEN( "a message" ) {
        std::string message = "hello,  world!";
        WHEN( "analyzed" ) {
            auto IC = crypto::string_ana::find_log_quad( message );
            THEN( "The score is correct" ) {
                REQUIRE( IC == Approx( -28.5677 ));
            }
        }
    }
}