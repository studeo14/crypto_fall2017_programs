/**
 * Implementation file
 */

#include "crypto.h"
#include "qgr.h"
#include <cmath>
#include <cctype>
#include <iostream>
#include <algorithm>
#include <limits>
#include <queue>

namespace crypto {
    /**
     * Anonymous utility functions
     */
    namespace {
        void foo() {}

        void clean_string( std::string &text, bool upper ) {
            //first remove non alpha chars
            text.erase( std::remove_if( text.begin(), text.end(), []( auto &item ) {
                return !std::isalpha( item );
            } ), text.end());
            //now to lower/upper
            std::transform( text.begin(), text.end(), text.begin(), ( upper ) ? ( ::toupper ) : ( ::tolower ));
        }

        inline double decision_probability( double new_key, double old_key, uint32_t i ) {
            return std::exp(( new_key - old_key ) / ( 10 - 0.002 * i ));
        }

        inline char char_mod( char a, char b ) {
            return a - ( a / b ) * b;
        }
    };

    namespace string_ana {
        double find_coincidence_index( std::string text ) {

            //clean
            clean_string( text, false );

            //first perform freuency analysis
            auto freqs = frequency_search( text );

            //now calc si
            double retVal = 0.0;
            for ( auto &pair: *freqs ) {
                double itemCount = std::get< uint32_t >( pair );
                retVal += ( itemCount * itemCount ) - itemCount;
            }
            //now divide by size^2 - size
            double size = text.size();
            retVal /= ( size * size ) - size;

            return retVal;
        }

        /**
         * Interactive algorithm that breaks a permutation cypher
         * @param cryptext text to break
         * @return original message
         */
        std::string break_perm( const std::string &cryptext ) {
            //get the string size
            const uint32_t size = cryptext.size();

            //loop through all factors of size
            //find factors
            std::vector< std::pair< uint32_t, uint32_t > > factors;
            auto sqrt = ( uint32_t ) std::sqrt( size );
            for ( uint16_t i = 1; i <= sqrt; i++ ) {
                if ( size % i == 0 && i != 1 ) {
                    //need i and size/i
                    uint32_t fac = size / i;
                    factors.emplace_back( fac, i );
                    factors.emplace_back( i, fac );
                }
            }
            //now loop through vector and try to make a message
            std::cout << "Cryptext Length: " << size << std::endl;
            std::cout << "Number of factors: " << factors.size() << std::endl;

            for ( std::pair< uint32_t, uint32_t > p: factors ) {
                std::cout << "Factor: (" << std::get< 0, uint32_t, uint32_t >( p ) << ','
                          << std::get< 1, uint32_t, uint32_t >( p ) << ')' << std::endl;
                std::string retVal = find_message( p, size, cryptext );
                std::cout << "Message: " << retVal << " . Accept (Y/N)? ";
                char answer;
                std::cin >> answer;
                if ( answer == 'y' || answer == 'Y' ) {
                    return retVal;
                }
            }
            return "";
        }

        /**
         * Algorithm to find decrypt a permuation cyphertext given the text, dimensions to use, and the size of the string
         * @param factors
         * @param size
         * @param cryptext
         * @return original message
         */
        std::string find_message( const std::pair< uint32_t, uint32_t > &factors, const uint32_t size,
                                  const std::string &cryptext ) {
            //make an array of characters as long as the cryptext
            //will be accessed via [row*n+col]
            std::vector< char > transform_matrix( size );
            //m and n
            uint32_t
                    m = std::get< 0, uint32_t, uint32_t >( factors ),
                    n = std::get< 1, uint32_t, uint32_t >( factors );

            std::string retVal;

            //build the message by reading  the cryptext
            for ( uint16_t col = 0; col < n; col++ ) {
                for ( uint16_t row = 0; row < m; row++ ) {
                    retVal.push_back( cryptext[ row * n + col ] );
                }
            }
            return retVal;
        };

        /**
         * Counts the frequency of the contained characters inside of the given message
         * This algorithm does NOT clean the given message; it will count all distinct characters in
         * the given message
         * @param message
         * @return pointer to a list of pairs (char, frequency)
         */
        freq_list_ptr frequency_search(
                std::string message ) {
            //create the map
            freq_map count_map;

            for ( char c: message ) {
                if ( count_map.find( c ) != count_map.end()) {
                    count_map[ c ]++;
                } else {
                    count_map[ c ] = 1;
                }
            }

            //construct list from map
            freq_list_ptr retVal = std::make_unique< freq_list >( count_map.begin(), count_map.end());

            std::sort( retVal->begin(), retVal->end(), []( auto &a, auto &b ) {
                return std::get< uint32_t >( a ) > std::get< uint32_t >( b );
            } );


            return retVal;
        }

        void print_freq_list( const freq_list &list ) {
            for ( auto &pair: list ) {
                std::cout << '(' << std::get< char >( pair ) << ',' << std::get< uint32_t >( pair ) << "), ";
            }
            std::cout << std::endl;
        }

        double find_log_quad( std::string text ) {
            //clean text
            //remove non-alpha and make UPPER
            clean_string( text, true );

            int i;
            char temp[4];
            double score = 0;
            for ( i = 0; i < text.size() - 3; i++ ) {
                temp[ 0 ] = text[ i ] - 'A';
                temp[ 1 ] = text[ i + 1 ] - 'A';
                temp[ 2 ] = text[ i + 2 ] - 'A';
                temp[ 3 ] = text[ i + 3 ] - 'A';
                // we have to index into the correct part of the array
                score += qgr::qgram[ 17576 * temp[ 0 ] + 676 * temp[ 1 ] + 26 * temp[ 2 ] + temp[ 3 ]];
            }
            return score;
        }

        //assumes a clean (no spaces, non aplha) text, also all lower case
        std::string shift_decrypt( std::string text, uint8_t key ) {
            std::transform( text.begin(), text.end(), text.begin(), [ &key ]( auto _c ) -> char {
                //sub key
                _c -= key;
                //check bounds
                if ( _c < 'a' ) {
                    //add 26 to adjust back
                    // a - 1 -> z
                    // a - 2 -> y
                    // ect
                    _c += 26;
                }
                return _c;
            } );

            return text;
        }

        std::string
        hill_with_anealing( const std::string &cryptext , const bool print) {
            uint8_t best_key = 0;
            double best_fitness = -std::numeric_limits< double >::max();
            //for use in loop
            std::string temp_string;
            double temp_fitness, probability;
            //find best key
            for ( uint8_t ix = 0; ix < 26; ix++ ) {
                //first find the decryption
                temp_string = shift_decrypt( cryptext, ix );

                //find fitness
                temp_fitness = find_log_quad( temp_string );

                //compare values
                if ( temp_fitness > best_fitness ) {
                    best_key = ix;
                    best_fitness = temp_fitness;
                } else {
                    //find probability of choosing to go down
                    probability = decision_probability( temp_fitness, best_fitness, ix );
                    if ( probability > 0.5 ) {
                        best_key = ix;
                        best_fitness = temp_fitness;
                    }
                    //else do nothing
                }
            }
            //now we have the best key and fitness
            if(print)
                std::cout << "Key: " << static_cast<char>(best_key + 'a') << std::endl;
            return shift_decrypt( cryptext, best_key );
        }

        std::string
        break_vigenere( std::string cryptext, const bool print) {
            //clean
            clean_string( cryptext, false );
            //frst find the key length
            uint32_t len = 1, best_len;
            double delta, best_delta = std::numeric_limits< double >::max();
            string_list_ptr cols;
            for ( ; len < cryptext.size() / 2; len++ ) {
                //find IC of each col
                //get cols
                cols = util::columnize( cryptext, len );
                double temp = 0.0;
                for ( auto str: *cols ) {
                    auto IC = find_coincidence_index( str );
                    temp += std::abs( IC - IC_ENGLISH );
                }
                temp /= cols->size();
                if ( temp < best_delta ) {
                    best_delta = temp;
                    best_len = len;
                }
            }
            //now we have the best key len
            //and the cols
            cols = util::columnize( cryptext, best_len );
            //decrypt each cols
            std::transform( cols->begin(), cols->end(), cols->begin(), [&print]( auto col ) {
                return hill_with_anealing( col, print );
            } );

            return util::decolumnize( cols );
        }
    };

    namespace util {

        template<>
        uint32_t get_value( const freq_list &list, const char &key ) {
            auto match_func = [ & ]( auto &item ) {
                return std::get< char >( item ) == key;
            };
            auto found_pair = std::find_if( list.begin(), list.end(), match_func );
            return std::get< uint32_t >( *found_pair );
        }

        string_list_ptr
        columnize( const std::string &text, uint32_t columns ) {
            auto retVal = std::make_unique< string_list >( columns );
            for ( uint32_t ix = 0; ix < text.size(); ix++ ) {
                retVal->at( ix % columns ).push_back( text[ ix ] );
            }
            return retVal;
        }

        std::string
        decolumnize( const string_list_ptr &cols ) {
            uint32_t size;
            std::for_each( cols->begin(), cols->end(), [ &size ]( auto col ) {
                size += col.size();
            } );
            std::string retVal;
            for ( uint32_t iy = 0; iy < cols->at( 0 ).size(); iy++ ) {
                for ( uint32_t ix = 0; ix < cols->size(); ix++ ) {
                    retVal.push_back( cols->at( ix )[ iy ] );
                }
            }
            return retVal;
        }
    };

    namespace math {

        /**
         * Returns the GCD of two input integers using the Euclidean algorithm
         * @param a
         * @param n needs to be greater than zero
         * @return gcd(a,n)
         */
        int32_t gcd( const int32_t &a, const int32_t &n ) {
            //base case
            if ( a == 0 )
                return n;
            //else

            return gcd( n % a, a );
        }

        /**
         * Override for other gcd function using a pair
         * @param values
         * @return gcd(values[0],values[1])
         */
        int32_t gcd( const std::pair< int32_t, int32_t > &values ) {
            return gcd(
                    std::get< 0, int32_t, int32_t >( values ),
                    std::get< 1, int32_t, int32_t >( values )
            );
        }

        /**
         * Implements the extended Euclidean algorithm that returns gcd(a,n)
         * as well as modifies x,y | ax + ny = gcd(a,n)
         * @param a
         * @param n
         * @param x
         * @param y
         * @return gcd(a,n)
         */
        int32_t gcd_extended( const int32_t &a, const int32_t &n, int32_t &x, int32_t &y ) {
            // Base Case
            if ( a == 0 ) {
                x = 0, y = 1;
                return n;
            }

            int32_t x1, y1; // To store results of recursive call
            int32_t gcd = gcd_extended( n % a, a, x1, y1 );

            // Update x and y using results of recursive
            // call
            x = y1 - ( n / a ) * x1;
            y = x1;

            return gcd;
        }

        /**
         * Overload of the other gcd_extended function using a pair for a,n
         * @param values
         * @param x
         * @param y
         * @return gcd(values[0],values[1])
         */
        int32_t gcd_extended( const std::pair< int32_t, int32_t > &values, int32_t &x, int32_t &y ) {
            return gcd_extended(
                    std::get< 0, int32_t, int32_t >( values ),
                    std::get< 1, int32_t, int32_t >( values ),
                    x,
                    y
            );
        }

        /**
         * Finds the modular inverse of an integer a mod m
         * @param a integer to find the inverse of
         * @param m mod to find the inverse within
         * @return MI, -1 if a,m are not coprime
         */
        int32_t modular_inverse( const int32_t &a, const int32_t &m ) {
            int32_t x, y;
            if ( gcd_extended( a, m, x, y ) != 1 ) {
                return -1;
            }
            //else
            return ( x % m + m ) % m;
        }
    };

    namespace lfsr {

        namespace {
            char
            xor_c( char a, char b ) {
                char retVal;
                a -= '0';
                b -= '0';
                retVal = a ^ b;
                retVal += '0';
                return retVal;
            }
        };

        void
        digest( std::ostringstream &out, std::istringstream &in, std::string seed ) {
            char current;
            while ( in >> current ) {
                //push to out
                out << xor_c( current, seed[ 0 ] );
                //get next bit
                char next = next_bit( seed );
                //pop
                seed.erase( 0, 1 );
                //add new
                seed.push_back( next );
            }
        }


        char
        next_bit( const std::string &seed ) {
            if ( seed.size() != 4 )
                return '\a';
            //first need to convert
            char
                    a3 = seed[ 3 ] - '0',
                    a1 = seed[ 1 ] - '0';
            //now get the retVal
            char retVal = a3 + a1;
            retVal &= 1; //mod 2
            retVal += '0'; //go back to char
            return retVal;
        }
    };
};