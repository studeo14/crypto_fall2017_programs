/**
 * Definition file for the Cryto functions library.
 */

#ifndef __SFRED_CRYPTO
#define __SFRED_CRYPTO

#include <string>
#include <map>
#include <utility>
#include <vector>
#include <memory>
#include <cstdint>
#include <sstream>

typedef std::map< char, uint32_t > freq_map;
typedef std::unique_ptr< freq_map > freq_map_ptr;
typedef std::vector< std::pair< char, uint32_t > > freq_list;
typedef std::unique_ptr< freq_list > freq_list_ptr;
typedef std::vector< std::string > string_list;
typedef std::unique_ptr< string_list > string_list_ptr;

namespace crypto {
    const double IC_ENGLISH = 0.0667;
    namespace string_ana {
        std::string break_perm( const std::string & );

        std::string
        find_message( const std::pair< uint32_t, uint32_t > &, const uint32_t, const std::string & );

        freq_list_ptr
        frequency_search( std::string );

        void
        print_freq_list( const freq_list & );

        double
        find_coincidence_index( std::string );

        double
        find_log_quad( std::string );

        std::string
        shift_decrypt( std::string, uint8_t );

        std::string
        hill_with_anealing( const std::string & , const bool );

        std::string
        break_vigenere( std::string, const bool);
    };

    namespace util {

        template< class R, class Container, class Key >
        R
        get_value( const Container &container, const Key & );

        string_list_ptr
        columnize( const std::string &, uint32_t );

        std::string
        decolumnize( const string_list_ptr & );
    };

    namespace math {
        int32_t
        gcd( const int32_t &, const int32_t & );

        int32_t
        gcd( const std::pair< int32_t, int32_t > & );

        int32_t
        gcd_extended( const int32_t &, const int32_t &, int32_t &, int32_t & );

        int32_t
        gcd_extended( const std::pair< int32_t, int32_t > &, int32_t &, int32_t & );

        int32_t
        modular_inverse( const int32_t &, const int32_t & );

    };

    namespace lfsr {
        void
        digest( std::ostringstream &, std::istringstream &, std::string );

        /**
         * Gets the next bit in the bit stream for
         * our lfsr
         * @param seed - previous four bits
         * @return 1*seed[3] + 0*seed[2] + 1*seed[1] + 0*seed[0]
         */
        char
        next_bit( const std::string &seed );
    };
};

#endif
