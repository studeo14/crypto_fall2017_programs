//
// Created by sfrederiksen on 9/14/17.
//

#include "crypto.h"
#include <iostream>

int main(int argv, char**argc){
    std::string chars;
    if(argv!=2){
        std::cout << "Enter text: " << std::endl;
        std::cin >> chars;
    }else{
        chars = argc[1];
    }

    std::cout << crypto::string_ana::break_vigenere(chars,  true) << std::endl;

    return 0;
}