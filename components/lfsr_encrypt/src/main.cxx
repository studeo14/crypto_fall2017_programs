//
// Created by sfrederiksen on 9/14/17.
//

#include "crypto.h"
#include <iostream>
#include <sstream>

int main(int argv, char**argc){
    std::string chars, seed;
    if(argv!=3){
        std::cerr << "Invalid number of params: needed 2 [string, seed]" << std::endl;
        return 1;
    }else{
        chars = argc[1];
        seed = argc[2];
    }

    std::istringstream in(chars);
    std::ostringstream out;

    crypto::lfsr::digest( out, in, seed );

    std::cout << out.str() << std::endl;

    return 0;
}
