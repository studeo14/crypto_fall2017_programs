//
// Created by sfrederiksen on 10/25/17.
//

#include "catch.hpp"
#include "aes.h"
#include <iostream>
#include <iomanip>
#include <bitset>

SCENARIO("Test creation of biad table", "[BT]"){
    GIVEN("an s-box"){
        const sBoxLookupTable_t sbox = {
            0x4,
            0x0,
            0xd,
            0x2,
            0xe,
            0x8,
            0xc,
            0xf,
            0x5,
            0x9,
            0x1,
            0x3,
            0xa,
            0xb,
            0x6,
            0x7
        };

        THEN("create the bias table"){
            auto bt = aes::util::make_bias_table(sbox);
            std::cout << std::setw(5) << " " ;
            for(uint8_t k = 0; k < 16; k++ ){
                std::cout << std::setw(5) << std::bitset<4>(k);
            }
            std::cout<<std::endl;
            for( uint16_t i = 0; i < bt->size(); i++ ){
                std::cout << std::setw(5) << std::left << std::bitset<4>(i) << std::right;
                for( uint16_t  j = 0; j < bt->at(i).size(); j++ ){
                    std::cout << std::setw(5) << (int)bt->at(i).at(j);
                }
                std::cout<<std::endl;
            }
        }
    }
}