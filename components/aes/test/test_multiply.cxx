//
// Created by sfrederiksen on 10/25/17.
//

#include "catch.hpp"
#include "aes.h"
#include <iostream>
#include <iomanip>
#include <bitset>
#include <string>

typedef std::vector< byte_set > unit;
typedef std::vector< unit > inout;

SCENARIO( "Test multiplication of polynomials mod AES' P", "[MULT]" ) {
    GIVEN( "Inputs and Expected Outputs" ) {
        inout tests = {
                { 0x3b, 0x16, 0x07 },
                { 0x99, 0x2F, 0xE0 },
                { 0x91, 0x6F, 0xE2 },
                { 0xE5, 0x77, 0x85 }
        };
        WHEN( "Inputs are multiplied" ) {
            for ( auto &i: tests ) {
                i.push_back(
                        std::stoi(
                                aes::util::multiply_and_reduce(
                                        byte_set_bits( i[ 0 ] ).to_string(),
                                        byte_set_bits( i[ 1 ] ).to_string()
                                ),
                                nullptr,
                                2
                        )
                );
            }

            THEN( "The results are as expected" ) {
                for ( auto &i: tests ) {
                    REQUIRE( i[ 2 ] == i[ 3 ] );
                }
            }
        }
    }
}