//
// Created by sfrederiksen on 10/25/17.
//

#include "catch.hpp"
#include "aes.h"
#include <iostream>
#include <iomanip>
#include <bitset>
#include <string>

typedef std::vector< std::string > unit;
typedef std::vector< unit > inout;

SCENARIO( "Test inversion of a polynomial mod AES' P", "[INV]" ) {
    GIVEN( "Inputs and Expected Outputs" ) {
        inout tests = {
                { "00001111", "11000111" },
                { "01001101", "00100101" },
                { "11000000", "00001011" },
                { "00000111", "11010001" },
        };
        WHEN( "Inputs are inverted" ) {
            for ( auto &i: tests ) {
                i.push_back(
                    aes::util::inverse(
                        i[ 0 ]
                    )
                );
            }

            THEN( "The results are as expected" ) {
                for ( auto &i: tests ) {
                    REQUIRE( i[ 2 ] == i[ 1 ] );
                }
            }
        }
    }
}