//
// Created by sfrederiksen on 10/25/17.
//

#include "aes.h"
#include <iostream>
#include <utility>
#include <algorithm>
#include <exception>
#include <iomanip>

#define B( b, n ) (b>>n)

namespace aes {
    namespace util {
        void println( aes_key_t vector, bool compact ) {
            //print each byteas two hex chars
            std::for_each( vector.begin(), vector.end(), [&compact]( auto c ) {
                if(compact)
                    std::cout << std::setfill( '0' ) << std::setw( 2 ) << std::hex << (int) c;
                else
                    std::cout << std::setfill( '0' ) << std::setw( 2 ) << std::hex << (int) c << ' ';

            } );
            std::cout << std::endl;
        }

        inline uint8_t poly_impl( uint8_t mask, uint8_t number ) {
            //find the number of ones in a polynomial from an sbox
            number &= mask;
            uint8_t res = (B( number, 3 ) & 1 ) ^(B( number, 2 ) & 1 ) ^(B( number, 1 ) & 1 ) ^( number & 1 );
            return res;
        }

        /**
        * i tells me which x bits to use
        * j tells me which y bits to use
        */
        int8_t getBias( const sBoxLookupTable_t &sBox, int16_t i, int16_t j ) {
            int8_t numberOfZeros = 0;
            uint8_t iMask = i & 0xf, jMask = j & 0xf;
            for ( int16_t entry = 0; entry < sBox.size(); entry++ ) {
                uint8_t res = poly_impl( iMask, entry ) ^poly_impl( jMask, sBox[ entry ] );
                if ( res == 0 )
                    numberOfZeros++;
            }
            return numberOfZeros - 8;

        }

        inline byte_set addByteSet( const byte_set &a, const byte_set &b ) {
            return a ^ b;
        }

        inline uint8_t addBytes( const uint8_t &a, const uint8_t &b ) {
            return a ^ b;
        }

        uint8_t degree( const byte_set &a ) {
            uint8_t retVal;
            byte_set_bits a_t( a );
            //start from the front and stop at the first 1
            for ( retVal = 15; retVal > 0; retVal-- ) {
                if ( a_t[ retVal ] )
                    break;
            }
            return retVal;
        }

        /**
         * Divide a by b and return both the result and the remainder
         * Essentially the euclidean division algorithm
         */
        std::pair< byte_set, byte_set > divide_poly( byte_set a, const byte_set &b ) {
            //find the degree of b
            uint8_t degree_b = degree( b );
            //find the degree of  a
            uint8_t degree_a = degree( a );
            //we will be returning the sum of all of the diffs
            byte_set diffSum = 0;
            //while a can be divided by b and a is positive
            while ( degree_a >= degree_b && a > 0 ) {
                //amount we can divide by
                uint8_t diff = degree_a - degree_b;
                //divisor to remove the most sig digit
                byte_set temp_b = b << diff;
                //remove
                a = addByteSet( a, temp_b );
                //refind the degree of  a
                degree_a = degree( a );
                //add diff bit
                diffSum += ( 1 << diff );
            }
            //result,remainder
            return std::make_pair( diffSum, a );

        }

        /**
         * @param a
         * @param p
         * @return
         */
        byte_set reduce( byte_set a, const byte_set &p ) {
            //can be implemented by finding the remainder of dividing by P
            return std::get< 1, byte_set, byte_set >( divide_poly( a, p ));
        }

        byte_set intermediate_multiply( const byte_set &x, const byte_set &y ) {
            //convert to bitset (easier to work with)
            byte_set_bits bBits( y );
            //then need to foil the two inputs
            std::vector< byte_set > foilResults;
            for ( unsigned ix = 0; ix < 8; ix++ ) {
                byte_set res( 0 );
                if ( bBits[ ix ] )
                    res = x << ix;
                foilResults.push_back( res );
            }
            //add them together (XOR)
            byte_set retVal = 0;
            for ( auto &item : foilResults ) {
                retVal = addByteSet( retVal, item );
            }
            //return the sum
            return retVal;
        }

        void poly_gcd_ext( const byte_set &mod, const byte_set &div, std::vector< byte_set > &results) {
            //get result of division
            auto reductionPair = divide_poly( mod, div );
            //seperate
            byte_set
                result = std::get< 0, byte_set, byte_set >( reductionPair ),
                remainder = std::get< 1, byte_set, byte_set >( reductionPair )
            ;
            //check result (base case)
            if ( remainder == 0 )
                return;
            //else
            //add to array
            results.push_back( result );
            //go to next step using the remainder
            poly_gcd_ext( div, remainder, results );
        }

        uint8_t rCon( uint8_t iter ) {
            //LUT
            static const uint8_t rcon[256] = {
                    0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a,
                    0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39,
                    0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a,
                    0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8,
                    0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef,
                    0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc,
                    0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80, 0x1b,
                    0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3,
                    0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94,
                    0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04, 0x08, 0x10, 0x20,
                    0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63, 0xc6, 0x97, 0x35,
                    0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd, 0x61, 0xc2, 0x9f,
                    0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d, 0x01, 0x02, 0x04,
                    0x08, 0x10, 0x20, 0x40, 0x80, 0x1b, 0x36, 0x6c, 0xd8, 0xab, 0x4d, 0x9a, 0x2f, 0x5e, 0xbc, 0x63,
                    0xc6, 0x97, 0x35, 0x6a, 0xd4, 0xb3, 0x7d, 0xfa, 0xef, 0xc5, 0x91, 0x39, 0x72, 0xe4, 0xd3, 0xbd,
                    0x61, 0xc2, 0x9f, 0x25, 0x4a, 0x94, 0x33, 0x66, 0xcc, 0x83, 0x1d, 0x3a, 0x74, 0xe8, 0xcb, 0x8d
            };

            return rcon[ iter ];
        }

        uint8_t getSBoxInvert( uint8_t num ) {
            //LUT
            static const uint8_t rsbox[256] =

                    { 0x52, 0x09, 0x6a, 0xd5, 0x30, 0x36, 0xa5, 0x38, 0xbf, 0x40, 0xa3, 0x9e, 0x81, 0xf3, 0xd7, 0xfb,
                      0x7c, 0xe3, 0x39, 0x82, 0x9b, 0x2f, 0xff, 0x87, 0x34, 0x8e, 0x43, 0x44, 0xc4, 0xde, 0xe9, 0xcb,
                      0x54, 0x7b, 0x94, 0x32, 0xa6, 0xc2, 0x23, 0x3d, 0xee, 0x4c, 0x95, 0x0b, 0x42, 0xfa, 0xc3, 0x4e,
                      0x08, 0x2e, 0xa1, 0x66, 0x28, 0xd9, 0x24, 0xb2, 0x76, 0x5b, 0xa2, 0x49, 0x6d, 0x8b, 0xd1, 0x25,
                      0x72, 0xf8, 0xf6, 0x64, 0x86, 0x68, 0x98, 0x16, 0xd4, 0xa4, 0x5c, 0xcc, 0x5d, 0x65, 0xb6, 0x92,
                      0x6c, 0x70, 0x48, 0x50, 0xfd, 0xed, 0xb9, 0xda, 0x5e, 0x15, 0x46, 0x57, 0xa7, 0x8d, 0x9d, 0x84,
                      0x90, 0xd8, 0xab, 0x00, 0x8c, 0xbc, 0xd3, 0x0a, 0xf7, 0xe4, 0x58, 0x05, 0xb8, 0xb3, 0x45, 0x06,
                      0xd0, 0x2c, 0x1e, 0x8f, 0xca, 0x3f, 0x0f, 0x02, 0xc1, 0xaf, 0xbd, 0x03, 0x01, 0x13, 0x8a, 0x6b,
                      0x3a, 0x91, 0x11, 0x41, 0x4f, 0x67, 0xdc, 0xea, 0x97, 0xf2, 0xcf, 0xce, 0xf0, 0xb4, 0xe6, 0x73,
                      0x96, 0xac, 0x74, 0x22, 0xe7, 0xad, 0x35, 0x85, 0xe2, 0xf9, 0x37, 0xe8, 0x1c, 0x75, 0xdf, 0x6e,
                      0x47, 0xf1, 0x1a, 0x71, 0x1d, 0x29, 0xc5, 0x89, 0x6f, 0xb7, 0x62, 0x0e, 0xaa, 0x18, 0xbe, 0x1b,
                      0xfc, 0x56, 0x3e, 0x4b, 0xc6, 0xd2, 0x79, 0x20, 0x9a, 0xdb, 0xc0, 0xfe, 0x78, 0xcd, 0x5a, 0xf4,
                      0x1f, 0xdd, 0xa8, 0x33, 0x88, 0x07, 0xc7, 0x31, 0xb1, 0x12, 0x10, 0x59, 0x27, 0x80, 0xec, 0x5f,
                      0x60, 0x51, 0x7f, 0xa9, 0x19, 0xb5, 0x4a, 0x0d, 0x2d, 0xe5, 0x7a, 0x9f, 0x93, 0xc9, 0x9c, 0xef,
                      0xa0, 0xe0, 0x3b, 0x4d, 0xae, 0x2a, 0xf5, 0xb0, 0xc8, 0xeb, 0xbb, 0x3c, 0x83, 0x53, 0x99, 0x61,
                      0x17, 0x2b, 0x04, 0x7e, 0xba, 0x77, 0xd6, 0x26, 0xe1, 0x69, 0x14, 0x63, 0x55, 0x21, 0x0c, 0x7d };
            return rsbox[ num ];
        }

        uint8_t getSBoxValue( uint8_t num ) {
            //LUT
            static const uint8_t sbox[256] =
                    { 0x63, 0x7c, 0x77, 0x7b, 0xf2, 0x6b, 0x6f, 0xc5, 0x30, 0x01, 0x67, 0x2b, 0xfe, 0xd7, 0xab, 0x76,
                      0xca, 0x82, 0xc9, 0x7d, 0xfa, 0x59, 0x47, 0xf0, 0xad, 0xd4, 0xa2, 0xaf, 0x9c, 0xa4, 0x72, 0xc0,
                      0xb7, 0xfd, 0x93, 0x26, 0x36, 0x3f, 0xf7, 0xcc, 0x34, 0xa5, 0xe5, 0xf1, 0x71, 0xd8, 0x31, 0x15,
                      0x04, 0xc7, 0x23, 0xc3, 0x18, 0x96, 0x05, 0x9a, 0x07, 0x12, 0x80, 0xe2, 0xeb, 0x27, 0xb2, 0x75,
                      0x09, 0x83, 0x2c, 0x1a, 0x1b, 0x6e, 0x5a, 0xa0, 0x52, 0x3b, 0xd6, 0xb3, 0x29, 0xe3, 0x2f, 0x84,
                      0x53, 0xd1, 0x00, 0xed, 0x20, 0xfc, 0xb1, 0x5b, 0x6a, 0xcb, 0xbe, 0x39, 0x4a, 0x4c, 0x58, 0xcf,
                      0xd0, 0xef, 0xaa, 0xfb, 0x43, 0x4d, 0x33, 0x85, 0x45, 0xf9, 0x02, 0x7f, 0x50, 0x3c, 0x9f, 0xa8,
                      0x51, 0xa3, 0x40, 0x8f, 0x92, 0x9d, 0x38, 0xf5, 0xbc, 0xb6, 0xda, 0x21, 0x10, 0xff, 0xf3, 0xd2,
                      0xcd, 0x0c, 0x13, 0xec, 0x5f, 0x97, 0x44, 0x17, 0xc4, 0xa7, 0x7e, 0x3d, 0x64, 0x5d, 0x19, 0x73,
                      0x60, 0x81, 0x4f, 0xdc, 0x22, 0x2a, 0x90, 0x88, 0x46, 0xee, 0xb8, 0x14, 0xde, 0x5e, 0x0b, 0xdb,
                      0xe0, 0x32, 0x3a, 0x0a, 0x49, 0x06, 0x24, 0x5c, 0xc2, 0xd3, 0xac, 0x62, 0x91, 0x95, 0xe4, 0x79,
                      0xe7, 0xc8, 0x37, 0x6d, 0x8d, 0xd5, 0x4e, 0xa9, 0x6c, 0x56, 0xf4, 0xea, 0x65, 0x7a, 0xae, 0x08,
                      0xba, 0x78, 0x25, 0x2e, 0x1c, 0xa6, 0xb4, 0xc6, 0xe8, 0xdd, 0x74, 0x1f, 0x4b, 0xbd, 0x8b, 0x8a,
                      0x70, 0x3e, 0xb5, 0x66, 0x48, 0x03, 0xf6, 0x0e, 0x61, 0x35, 0x57, 0xb9, 0x86, 0xc1, 0x1d, 0x9e,
                      0xe1, 0xf8, 0x98, 0x11, 0x69, 0xd9, 0x8e, 0x94, 0x9b, 0x1e, 0x87, 0xe9, 0xce, 0x55, 0x28, 0xdf,
                      0x8c, 0xa1, 0x89, 0x0d, 0xbf, 0xe6, 0x42, 0x68, 0x41, 0x99, 0x2d, 0x0f, 0xb0, 0x54, 0xbb, 0x16 };
            return sbox[ num ];
        }

        void expansionCore( aes_key_t &temp, uint8_t iter ) {
            //rotate left
            std::rotate( temp.begin(), temp.begin() + 1, temp.end());
            //subbytes
            for ( byte_set &ix : temp ) {
                ix = getSBoxValue( ix );
            }
            //RCON XOR with first byte only
            temp[ 0 ] = rCon( iter ) ^ temp[ 0 ];
        }

        void addRoundKey( aes_key_t &state, const aes_key_t &key ) {
            //"zip" the two arrays together, using the first as the destination
            //first two args are the first vector, third is the second vector, last is the destination
            std::transform( state.begin(), state.end(), key.begin(), state.begin(),
                //apply this to each pair of values
                []( const auto &a, const auto &b ) -> auto {
                    return addByteSet( a, b );
                }
            );
        }

        void subBytes( aes_key_t &state ) {
            //apply to each
            for ( auto &byte: state ) {
                byte = getSBoxValue( byte );
            }
        }

        void invSubBytes( aes_key_t &state ) {
            //apply to each
            for ( auto &byte: state ) {
                byte = getSBoxInvert( byte );
            }
        }

        void getRoundKey( aes_key_t &target, const aes_key_t &keySchedule, uint16_t round ) {
            //which block to get
            uint16_t offset = round * 16;
            //just inline copy it into the target
            std::copy( keySchedule.begin() + offset, keySchedule.begin() + offset + 16, target.begin());
        }

        void shiftRows( aes_key_t &state ) {
            //ignore zeroth row
            //first row (by 1)
            std::rotate( state.begin() + 4, state.begin() + 4 + 1, state.begin() + 8 );
            //second row (by 2)
            std::rotate( state.begin() + 8, state.begin() + 8 + 2, state.begin() + 12 );
            //third row (by 3)
            std::rotate( state.begin() + 12, state.begin() + 12 + 3, state.begin() + 16 );
        }

        void invShiftRows( aes_key_t &state ) {
            //note: rbegin starts from the back and increments forwards
            //third row (by 3)
            std::rotate( state.rbegin(), state.rbegin() + 3, state.rbegin() + 4 );
            //second row (by 2)
            std::rotate( state.rbegin() + 4, state.rbegin() + 4 + 2, state.rbegin() + 8 );
            //first row (by 1)
            std::rotate( state.rbegin() + 8, state.rbegin() + 8 + 1, state.rbegin() + 12 );
            //ignore zeroth row
        }

        void mixColumns( aes_key_t &state ) {
            byte_set t1( 0 ), t2( 0 ), t3( 0 ), t4( 0 );
            //no need to use intermediate_multiply here as these multiplications are very simple
            //for each column
            for ( uint8_t i = 0; i < 4; i++ ) {
                //find the first new value (dot product)
                t1 = reduce(
                        state[ 0 * 4 + i ] << 1 ^
                        state[ 1 * 4 + i ] << 1 ^ state[ 1 * 4 + i ] ^
                        state[ 2 * 4 + i ] ^
                        state[ 3 * 4 + i ]
                );
                //find the second new value (dot product)
                //2
                t2 = reduce(
                        state[ 0 * 4 + i ] ^
                        state[ 1 * 4 + i ] << 1 ^
                        state[ 2 * 4 + i ] << 1 ^ state[ 2 * 4 + i ] ^
                        state[ 3 * 4 + i ]
                );
                //find the third new value (dot product)
                //3
                t3 = reduce(
                        state[ 0 * 4 + i ] ^
                        state[ 1 * 4 + i ] ^
                        state[ 2 * 4 + i ] << 1 ^
                        state[ 3 * 4 + i ] << 1 ^ state[ 3 * 4 + i ]
                );
                //find the fourth new value (dot product)
                //4
                t4 = reduce(
                        state[ 0 * 4 + i ] << 1 ^ state[ 0 * 4 + i ] ^
                        state[ 1 * 4 + i ] ^
                        state[ 2 * 4 + i ] ^
                        state[ 3 * 4 + i ] << 1
                );
                //apply to state
                state[ 0 * 4 + i ] = t1;
                state[ 1 * 4 + i ] = t2;
                state[ 2 * 4 + i ] = t3;
                state[ 3 * 4 + i ] = t4;
            }
        }

        void invMixColumns( aes_key_t &state ) {
            byte_set t1( 0 ), t2( 0 ), t3( 0 ), t4( 0 );
            //for each column
            for ( uint8_t i = 0; i < 4; i++ ) {
                //dot product of inverse matrix
                t1 = reduce(
                        intermediate_multiply( state[ 0 * 4 + i ], 0xE ) ^
                        intermediate_multiply( state[ 1 * 4 + i ], 0xB ) ^
                        intermediate_multiply( state[ 2 * 4 + i ], 0xD ) ^
                        intermediate_multiply( state[ 3 * 4 + i ], 0x9 )
                );
                //2
                t2 = reduce(
                        intermediate_multiply( state[ 0 * 4 + i ], 0x9 ) ^
                        intermediate_multiply( state[ 1 * 4 + i ], 0xE ) ^
                        intermediate_multiply( state[ 2 * 4 + i ], 0xB ) ^
                        intermediate_multiply( state[ 3 * 4 + i ], 0xD )
                );
                //3
                t3 = reduce(
                        intermediate_multiply( state[ 0 * 4 + i ], 0xD ) ^
                        intermediate_multiply( state[ 1 * 4 + i ], 0x9 ) ^
                        intermediate_multiply( state[ 2 * 4 + i ], 0xE ) ^
                        intermediate_multiply( state[ 3 * 4 + i ], 0xB )
                );
                //4
                t4 = reduce(
                        intermediate_multiply( state[ 0 * 4 + i ], 0xB ) ^
                        intermediate_multiply( state[ 1 * 4 + i ], 0xD ) ^
                        intermediate_multiply( state[ 2 * 4 + i ], 0x9 ) ^
                        intermediate_multiply( state[ 3 * 4 + i ], 0xE )
                );
                state[ 0 * 4 + i ] = t1;
                state[ 1 * 4 + i ] = t2;
                state[ 2 * 4 + i ] = t3;
                state[ 3 * 4 + i ] = t4;
            }
        }

        void rearrangeBlock( aes_key_t &block ) {
            //initially in row, col order
            //need to move to col, row order
            for ( unsigned x = 0; x < 4; x++ ) {
                for ( unsigned y = 0; y < 4; y++ ) {
                    //add to the end in new order
                    block.push_back( block[ y * 4 + x ] );
                }
            }
            //remove old entries (move new to front of the vector)
            std::rotate( block.begin(), block.begin() + 16, block.end());
            //shrink back to size (remove last half and old values)
            block.resize( 16 );
        }

        /**
         * for all combos 0-F x 0-F go through all sbox entries and xor them
         * then count number of 0 results and assign that to the map
         *
         * simple 4 bit impl
         *
         */
        mat_t_ptr make_bias_table( const sBoxLookupTable_t &sBox ) {
            //make the map
            //size
            size_t size = sBox.size();
            mat_t_ptr table = std::make_unique< mat_t >( size, sBoxLookupTable_t( size ));

            for ( int16_t i = 0; i < size; i++ ) {
                for ( int16_t j = 0; j < size; j++ ) {
                    if ( i == 0 && j == 0 ) {
                        table->at( i ).at( j ) = 0;
                    } else {
                        //else
                        table->at( i ).at( j ) = getBias( sBox, i, j );
                    }
                }
            }

            return table;
        }

        std::string multiply( const std::string &x, const std::string &y ) {
            //first need to convert ot bitset
            byte_set
                    a = std::stoi( x, nullptr, 2 ),
                    b = std::stoi( y, nullptr, 2 );
            //convert to string then return
            return std::bitset< 8 >( intermediate_multiply( a, b )).to_string();

        }

        std::string multiply_and_reduce( const std::string &x, const std::string &y, const std::string &p ) {
            //first need to convert ot bitset
            byte_set
                    a = std::stoi( x, nullptr, 2 ),
                    b = std::stoi( y, nullptr, 2 ),
                    mod = std::stoi( p, nullptr, 2 );
            byte_set retVal = reduce( intermediate_multiply( a, b ), mod );
            //convert to string then return
            return std::bitset< 8 >( retVal ).to_string();

        }

        std::string inverse( const std::string &x, const std::string &p ) {
            //first need to convert to ints
            byte_set
                    poly = std::stoi( x, nullptr, 2 ),
                    mod = std::stoi( p, nullptr, 2 );

            //then need to create the a_i array
            std::vector< byte_set > a;
            //recurse and populate the array
            //(dividing mod by poly until 0)
            poly_gcd_ext( mod, poly, a );

            //now find k_(n-1)
            byte_set k0( 0 ), k1( 1 ), next_k( 0 );

            //for each a
            for ( auto &a_i: a ) {
                next_k = addByteSet( k0, intermediate_multiply( k1, a_i ));
                k0 = k1;
                k1 = next_k;
            }
            //result is next_k (reduced mod p)
            return std::bitset< 8 >( reduce( next_k, mod )).to_string();
        }

        aes_key_ptr_t key128Expand( const aes_key_t &key ) {
            //check for size
            if ( key.size() != 16 )
                throw std::invalid_argument("Key is not 128 bits");
            //else do expansion
            aes_key_ptr_t retVal = std::make_unique< aes_key_t >( key );
            //start at 1 because already have first key
            uint16_t iter = 1;
            //
            while ( retVal->size() < 176 ) {
                //get first 4 of previous key
                aes_key_t temp1( retVal->end() - 4, retVal->end());
                //apply rcon if needed
                if ( retVal->size() % 16 == 0 ) {
                    expansionCore( temp1, iter );
                    iter++;
                }
                //get temp 2 (last 4 of first 16)
                aes_key_t temp2( retVal->end() - 16, retVal->end());
                temp2.resize( 4 ); //first 4 of the last 16
                //now xor and push each byte in the temps
                for ( uint8_t k = 0; k < 4; k++ ) {
                    retVal->push_back( temp1[ k ] ^ temp2[ k ] );
                }
            }
            return retVal;
        }

        aes_key_ptr_t key192Expand( const aes_key_t &key ) {
            //check for size
            if ( key.size() != 24 )
                throw std::invalid_argument("Key is not 192 bits");
            //else do expansion
            aes_key_ptr_t retVal = std::make_unique< aes_key_t >( key );
            uint16_t iter = 1;
            while ( retVal->size() < 208 ) {
                aes_key_t temp1( retVal->end() - 4, retVal->end());
                if ( retVal->size() % 24 == 0 ) {
                    expansionCore( temp1, iter );
                    iter++;
                }
                aes_key_t temp2( retVal->end() - 24, retVal->end());
                temp2.resize( 4 ); //first 4 of the last 24
                //now xor and push each byte in the temps
                for ( uint8_t k = 0; k < 4; k++ ) {
                    retVal->push_back( temp1[ k ] ^ temp2[ k ] );
                }
            }
            return retVal;
        }

        aes_key_ptr_t key256Expand( const aes_key_t &key ) {
            //check for size
            if ( key.size() != 32 )
                throw std::invalid_argument("Key is not 256 bits");
            //else do expansion
            auto retVal = std::make_unique< aes_key_t >( key );
            uint16_t iter = 1;
            while ( retVal->size() < 240 ) {
                //first 4
                aes_key_t temp1( retVal->end() - 4, retVal->end());
                //rcon
                if ( retVal->size() % 32 == 0 ) {
                    expansionCore( temp1, iter );
                    iter++;
                }
                //extra sbox
                if ( retVal->size() % 32 == 16 ) {
                    for ( auto &byte: temp1 ) {
                        byte = getSBoxValue( byte );
                    }
                }
                aes_key_t temp2( retVal->end() - 32, retVal->end());
                temp2.resize( 4 ); //first 4 of the last 32
                //now xor and push each byte in the temps
                for ( uint8_t k = 0; k < 4; k++ ) {
                    retVal->push_back( temp1[ k ] ^ temp2[ k ] );
                }
            }
            return retVal;
        }

        aes_key_ptr_t stringToStream( const std::string &string ) {
            auto retVal = std::make_unique< aes_key_t >();
            std::copy( string.begin(), string.end(), std::back_inserter( *retVal ));
            return retVal;
        }
    };

    namespace encryptors {
        encrypt_ecb::encrypt_ecb() : m_key(), m_state( 16 ) {}

        encrypt_ecb::encrypt_ecb( const aes_key_t &key ) : m_key( key ), m_state( 16 ) {
            m_keyLength = m_key.size();
            //init expansion
            //ignore exception and pass it up
            switch ( m_keyLength ) {
                case 16:
                    m_expanded_key = util::key128Expand( m_key );
                    break;
                case 24:
                    m_expanded_key = util::key192Expand( m_key );
                    break;
                case 32:
                    m_expanded_key = util::key256Expand( m_key );
                    break;
                default:
                    throw std::invalid_argument( "Unsupported Key Length. Need 128, 192, or 256 bits." );
            }
        }

        void encrypt_ecb::encrypt( const aes_key_t &block, uint8_t rounds ) {
            //state
            std::copy( block.begin(), block.end(), m_state.begin());
            //arrange in col, row order
            util::rearrangeBlock( m_state );
            /*
            * inital round:
            * addRoundKey
            */
            //round key
            uint8_t round( 0 );
            aes_key_t roundKey( 16 );
            util::getRoundKey( roundKey, *m_expanded_key, round );
            util::rearrangeBlock( roundKey );
            util::addRoundKey( m_state, roundKey );
            /*
            * rounds - 1 rounds of:
            * subBytes
            * shiftRows
            * mixColumns
            * addRoundKey
            */
            for ( round = 1; round < rounds; ++round ) {
                util::subBytes( m_state );
                util::shiftRows( m_state );
                util::mixColumns( m_state );
                util::getRoundKey( roundKey, *m_expanded_key, round );
                util::rearrangeBlock( roundKey );
                util::addRoundKey( m_state, roundKey );
            }
            /*
            * final round:
            * subBytes
            * shiftRows
            * addRoundKey
            */
            util::getRoundKey( roundKey, *m_expanded_key, round );
            util::rearrangeBlock( roundKey );
            util::subBytes( m_state );
            util::shiftRows( m_state );
            util::addRoundKey( m_state, roundKey );

            //return to original ordering
            util::rearrangeBlock( m_state );
        }

        aes_key_ptr_t encrypt_ecb::encrypt_block( const aes_key_t &block ) {
            //make sure size is correct
            if ( block.size() != 16 ) {
                throw std::invalid_argument( "Block size not 128 bits" );
            }
            //then find which type of key
            switch ( m_keyLength ) {
                case 16:
                    //encrypt with 10 rounds
                    encrypt( block, 10 );
                    break;
                case 24:
                    //encrypt with 12 rounds
                    encrypt( block, 12 );
                    break;
                case 32:
                    //encrypt with 14 rounds
                    encrypt( block, 14 );
                    break;
                default:
                    throw std::invalid_argument( "Key not initialized correctly" );
            }
            //here then valid
            auto retVal = std::make_unique< aes_key_t >( m_state );
            return retVal;
        }

        aes_key_ptr_t encrypt_ecb::encrypt_stream( const aes_key_t &stream ) {
            //get number of blocks and amount of needed padding
            size_t paddingGap = stream.size() % 16;
            size_t fullBlocks = stream.size() / 16;
            //create retVal
            auto retVal = std::make_unique< aes_key_t >();
            aes_key_t currentPT( 16 );
            //for each full block
            size_t currentBlock;
            for ( currentBlock = 0; currentBlock < fullBlocks; currentBlock++ ) {
                //get block to use
                util::getRoundKey( currentPT, stream, currentBlock );
                auto temp_result = encrypt_block( currentPT );
                //append
                std::copy( temp_result->begin(), temp_result->end(), std::back_inserter( *retVal ));
            }
            //extra bytes
            if ( paddingGap ) { //if there is extra
                //fill with zeros (padding)
                for ( auto &a: currentPT ) {
                    a = 0x00;
                }
                //fill up as much as possible
                util::getRoundKey( currentPT, stream, currentBlock );
                auto temp_result = encrypt_block( currentPT );
                //append
                std::copy( temp_result->begin(), temp_result->end(), std::back_inserter( *retVal ));
            }
            return retVal;
        }

        aes_key_ptr_t encrypt_ecb::encrypt_stream( const std::string &string ) {
            //just convert to a stream of bytes
            auto stream = util::stringToStream( string );
            //then run regular function
            return encrypt_stream( *stream );
        }

        aes_key_ptr_t encrypt_cbc::encrypt_stream( const aes_key_t &stream, const aes_key_t &IV ) {
            //get number of blocks and amount of needed padding
            size_t paddingGap = stream.size() % 16;
            size_t fullBlocks = stream.size() / 16;
            //create retVal
            auto retVal = std::make_unique< aes_key_t >();
            aes_key_t currentPT( 16 );
            //intermediate passthrough value for cbc
            auto intermediate_result = std::make_unique< aes_key_t >();
            //for each full block
            size_t currentBlock;
            for ( currentBlock = 0; currentBlock < fullBlocks; currentBlock++ ) {
                //get block to use
                util::getRoundKey( currentPT, stream, currentBlock );
                //if first then add IV
                if ( currentBlock == 0 ) {
                    util::addRoundKey( currentPT, IV );
                } else { //add previous result
                    util::addRoundKey( currentPT, *intermediate_result );
                }
                //encrypt the block
                intermediate_result = encrypt_block( currentPT );
                //append
                std::copy( intermediate_result->begin(), intermediate_result->end(), std::back_inserter( *retVal ));
            }
            //extra bytes
            if ( paddingGap ) { //if there is extra
                //fill with zeros (padding)
                for ( auto &a: currentPT ) {
                    a = 0x00;
                }
                //fill up as much as possible
                util::getRoundKey( currentPT, stream, currentBlock );
                //add previous result
                util::addRoundKey( currentPT, *intermediate_result );
                //encrypt
                intermediate_result = encrypt_block( currentPT );
                //append
                std::copy( intermediate_result->begin(), intermediate_result->end(), std::back_inserter( *retVal ));
            }
            return retVal;
        }

        aes_key_ptr_t encrypt_cbc::encrypt_stream( const std::string &string, const aes_key_t &IV ) {
            auto stream = util::stringToStream( string );
            return encrypt_stream( *stream, IV );
        }
    };

    namespace decryptors {
        decrypt_ecb::decrypt_ecb() : m_key(), m_state( 16 ) {}

        decrypt_ecb::decrypt_ecb( const aes_key_t &key ) : m_key( key ), m_state( 16 ) {
            m_keyLength = m_key.size();
            //init expansion
            switch ( m_keyLength ) {
                case 16:
                    m_expanded_key = util::key128Expand( m_key );
                    break;
                case 24:
                    m_expanded_key = util::key192Expand( m_key );
                    break;
                case 32:
                    m_expanded_key = util::key256Expand( m_key );
                    break;
                default:
                    throw std::invalid_argument( "Unsupported Key Length. Need 128, 192, or 256 bits." );
            }
        }

        void decrypt_ecb::decrypt( const aes_key_t &block, uint8_t rounds ) {
            //state
            std::copy( block.begin(), block.end(), m_state.begin());
            //arrange in col, row order
            util::rearrangeBlock( m_state );
            /*
            * inital round:
            * addRoundKey
            * invShiftRows
            * invSubBytes
            */
            //round key
            uint8_t round( rounds );
            aes_key_t roundKey( 16 );
            util::getRoundKey( roundKey, *m_expanded_key, round );
            util::rearrangeBlock( roundKey );
            util::addRoundKey( m_state, roundKey );
            util::invShiftRows( m_state );
            util::invSubBytes( m_state );
            /*
            * rounds - 1 rounds of:
            * addRoundKey
            * invMixColumns
            * invShiftRows
            * invSubBytes
            */
            for ( round = rounds - 1; round > 0; --round ) {
                util::getRoundKey( roundKey, *m_expanded_key, round );
                util::rearrangeBlock( roundKey );
                util::addRoundKey( m_state, roundKey );
                util::invMixColumns( m_state );
                util::invShiftRows( m_state );
                util::invSubBytes( m_state );
            }
            /*
            * final round:
            * addRoundKey
            */
            util::getRoundKey( roundKey, *m_expanded_key, round );
            util::rearrangeBlock( roundKey );
            util::addRoundKey( m_state, roundKey );

            //return to original ordering
            util::rearrangeBlock( m_state );
        }

        aes_key_ptr_t decrypt_ecb::decrypt_block( const aes_key_t &block ) {
            //make sure size is correct
            if ( block.size() != 16 ) {
                throw std::invalid_argument( "Block size not 128 bits" );
            }
            //then find which type of key
            switch ( m_keyLength ) {
                case 16:
                    //decrypt with 10 rounds
                    decrypt( block, 10 );
                    break;
                case 24:
                    //decrypt with 12 rounds
                    decrypt( block, 12 );
                    break;
                case 32:
                    //decrypt with 14 rounds
                    decrypt( block, 14 );
                    break;
                default:
                    throw std::invalid_argument( "Key not initialized correctly" );
            }
            //here then valid
            auto retVal = std::make_unique< aes_key_t >( m_state );
            return retVal;
        }

        aes_key_ptr_t decrypt_ecb::decrypt_stream( const aes_key_t &stream ) {
            //get number of blocks and amount of needed padding
            size_t paddingGap = stream.size() % 16;
            size_t fullBlocks = stream.size() / 16;
            //create retVal
            auto retVal = std::make_unique< aes_key_t >();
            aes_key_t currentPT( 16 );
            //for each full block
            size_t currentBlock;
            for ( currentBlock = 0; currentBlock < fullBlocks; currentBlock++ ) {
                //get block to use
                util::getRoundKey( currentPT, stream, currentBlock );
                auto temp_result = decrypt_block( currentPT );
                //append
                std::copy( temp_result->begin(), temp_result->end(), std::back_inserter( *retVal ));
            }
            //extra bytes
            if ( paddingGap ) { //if there is extra
                //fill with zeros (padding)
                for ( auto &a: currentPT ) {
                    a = 0x00;
                }
                //fill up as much as possible
                util::getRoundKey( currentPT, stream, currentBlock );
                auto temp_result = decrypt_block( currentPT );
                //append
                std::copy( temp_result->begin(), temp_result->end(), std::back_inserter( *retVal ));
            }

            return retVal;
        }

        aes_key_ptr_t decrypt_ecb::decrypt_stream( const std::string &string ) {
            auto stream = util::stringToStream( string );
            return decrypt_stream( *stream );
        }

        aes_key_ptr_t decrypt_cbc::decrypt_stream( const aes_key_t &stream, const aes_key_t &IV ) {
            //get number of blocks and amount of needed padding
            size_t paddingGap = stream.size() % 16;
            size_t fullBlocks = stream.size() / 16;
            //create retVal
            auto retVal = std::make_unique< aes_key_t >();
            aes_key_t currentCT( 16 );
            aes_key_t previousCT( 16 );
            //intermediate passthrough value for cbc
            auto tempState = std::make_unique< aes_key_t >();
            //for each full block
            size_t currentBlock;
            for ( currentBlock = 0; currentBlock < fullBlocks; currentBlock++ ) {
                //get block to use
                util::getRoundKey( currentCT, stream, currentBlock );
                tempState = decrypt_block( currentCT );
                //if first then add IV
                if ( currentBlock == 0 ) {
                    util::addRoundKey( *tempState, IV );
                } else { //add previous ciphertext
                    util::addRoundKey( *tempState, previousCT );
                }
                //append
                std::copy( tempState->begin(), tempState->end(), std::back_inserter( *retVal ));
                //pass forward for CBC
                std::copy(currentCT.begin(),currentCT.end(),previousCT.begin());
            }
            //extra bytes
            if ( paddingGap ) { //if there is extra
                //fill with zeros (padding)
                for ( auto &a: currentCT ) {
                    a = 0x00;
                }
                //fill up as much as possible
                util::getRoundKey( currentCT, stream, currentBlock );
                //decrypt
                tempState = decrypt_block( currentCT );
                //add previous result
                util::addRoundKey( *tempState, previousCT );
                //append
                std::copy( tempState->begin(), tempState->end(), std::back_inserter( *retVal ));
            }

            return retVal;

        }

        aes_key_ptr_t decrypt_cbc::decrypt_stream( const std::string &string, const aes_key_t &IV ) {
            auto stream = util::stringToStream( string );
            return decrypt_stream( *stream, IV );
        }
    }
};
