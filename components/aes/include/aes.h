//
// Created by sfrederiksen on 10/25/17.
//

#ifndef CRYPTO_PROGRAMS_AES_H
#define CRYPTO_PROGRAMS_AES_H

#include <vector>
#include <cstdint>
#include <memory>
#include <bitset>

typedef std::vector< int8_t > sBoxLookupTable_t;
typedef std::vector< sBoxLookupTable_t > mat_t;
typedef std::unique_ptr< mat_t > mat_t_ptr;
typedef uint16_t byte_set;
typedef std::bitset< 16 > byte_set_bits;
typedef std::vector< byte_set > aes_key_t;
typedef std::unique_ptr< aes_key_t > aes_key_ptr_t;

/**
 * Main namespace for aes functions and classes
 */
namespace aes {
    //finite field constants
    const std::string P = "100011011";
    const uint16_t P_val = 0x11B;

    //cbc encryption default IV
    const aes_key_t def_IV = { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0 };

    /**
     * Utility functions that are the building blocks of AES
     */
    namespace util {
        //print out a block
        void println( aes_key_t vector, bool compact=false );

        //helper function for inverse
        uint8_t poly_impl( uint8_t mask, uint8_t number );

        //helper function for bias table
        int8_t getBias( const sBoxLookupTable_t &sBox, int16_t i, int16_t j );

        //XOR two 16-bit ints
        byte_set addByteSet( const byte_set &a, const byte_set &b );

        //XOR two bytes
        uint8_t addBytes( const uint8_t &a, const uint8_t &b );

        //interpret short int as a polynomial and return its degree
        uint8_t degree( const byte_set &a );

        //return the result and remainder of polynimial division
        std::pair< byte_set, byte_set > divide_poly( byte_set a, const byte_set &b );

        //reduce a polynomial mod P
        byte_set reduce( byte_set a, const byte_set &p = P_val );

        //multiply two polynomials
        byte_set intermediate_multiply( const byte_set &x, const byte_set &y );

        //get the result vector from the euclidean algorithm
        void poly_gcd_ext( const byte_set &mod, const byte_set &div, std::vector< byte_set > &a );

        //find the round constant for an interation
        uint8_t rCon( uint8_t iter );

        //inverse of the AES sbox
        uint8_t getSBoxInvert( uint8_t num );

        //AES sbox
        uint8_t getSBoxValue( uint8_t num );

        /**
         * apply the expansion core to temp (assumes 4bytes) for round iter
         */
        void expansionCore( aes_key_t &temp, uint8_t iter );

        /**
         * xor a key to a block state
         */
        void addRoundKey( aes_key_t &state, const aes_key_t &key );

        /**
         * apply the AES sbox to a block state
         */
        void subBytes( aes_key_t &state );

        /**
         * Apply the inverse AES sbox to the block state
         * @param state
         */
        void invSubBytes( aes_key_t &state );

        /**
         * Extract a block from a key schedule. Can be used to get a block from a
         * stream of blocks.
         * @param dest - where to put the extracted block
         * @param keySchedule - key schedule
         * @param round - which block to extract
         */
        void getRoundKey( aes_key_t &dest, const aes_key_t &keySchedule, uint16_t round );

        /**
         * Shift the rows of a state
         * @param state
         */
        void shiftRows( aes_key_t &state );

        /**
         * Inverse the shift rows of a state
         * @param state
         */
        void invShiftRows( aes_key_t &state );

        /**
         * Apply the mix columns step to state
         * @param state
         */
        void mixColumns( aes_key_t &state );

        /**
         * Inverse the mix columns step to state
         * @param state
         */
        void invMixColumns( aes_key_t &state );

        /**
         * Transform a block from row,col to col,row ordering
         * This function is its own bijection
         * @param block
         */
        void rearrangeBlock( aes_key_t &block );

        /**
         * Creates a bias table from a given sbox lookup table
         * @param sBox lookup table
         * @return a sBox.len()^2 size matrix of bias values x | x/sBox.len() is the bias
         */
        mat_t_ptr make_bias_table( const sBoxLookupTable_t &sBox );

        /**
         * Multiply two polynomials
         * @param x
         * @param y
         * @return
         */
        std::string multiply( const std::string &x, const std::string &y );

        /**
         * Multiply two polynomials mod P
         * @param x
         * @param y
         * @param p
         * @return
         */
        std::string multiply_and_reduce( const std::string &x, const std::string &y, const std::string &p = P );

        /**
         * Find the inverse of a polynomial mod P
         * @param x
         * @param p
         * @return
         */
        std::string inverse( const std::string &x, const std::string &p = P );

        /**
         * Find the key expansion of a 128-bit key
         * @return
         */
        aes_key_ptr_t key128Expand( const aes_key_t & );

        /**
         * Find the key expansion of a 192-bit key
         * @return
         */
        aes_key_ptr_t key192Expand( const aes_key_t & );

        /**
         * Find the key expansion of a 256-bit key
         * @return
         */
        aes_key_ptr_t key256Expand( const aes_key_t & );

        /**
         * Convert a string to a stream of bytes
         * @return
         */
        aes_key_ptr_t stringToStream( const std::string & );

    };

    /**
     * Encryption and decryption classes
     */
    namespace encryptors {
        /**
         * Abstract encryptor class
         */
        class encryptor {
        protected:
            /**
             * Applies the AES algorithm to a block, takes the number of rounds to use
             * @param block
             * @param rounds
             */
            virtual void encrypt( const aes_key_t &block, uint8_t rounds ) = 0;

        public:
            /**
             * Encrypt a single block
             * @return
             */
            virtual aes_key_ptr_t encrypt_block( const aes_key_t & ) = 0;

            /**
             * Encrypt a stream of bytes
             * @return
             */
            virtual aes_key_ptr_t encrypt_stream( const aes_key_t & ) = 0;

            /**
             * Encrypt a string
             * @return
             */
            virtual aes_key_ptr_t encrypt_stream( const std::string & ) = 0;
        };

        /**
         * ECB AES Encryption
         */
        class encrypt_ecb : public encryptor {
        protected:
            //variables
            aes_key_t m_key;

            aes_key_ptr_t m_expanded_key;

            aes_key_t m_state;

            size_t m_keyLength;

            virtual void encrypt( const aes_key_t &block, uint8_t rounds );

        public:
            encrypt_ecb();

            explicit encrypt_ecb( const aes_key_t &key );

            virtual ~encrypt_ecb() {}

            virtual aes_key_ptr_t encrypt_block( const aes_key_t & );

            virtual aes_key_ptr_t encrypt_stream( const aes_key_t & );

            virtual aes_key_ptr_t encrypt_stream( const std::string & );
        };

        /**
         * CBC Encryption
         */
        class encrypt_cbc : public encrypt_ecb {
        public:
            explicit encrypt_cbc( const aes_key_t &key )
                    : encrypt_ecb( key ) {}

            virtual ~encrypt_cbc() {}

            virtual aes_key_ptr_t encrypt_stream( const aes_key_t &, const aes_key_t &IV = def_IV );

            virtual aes_key_ptr_t encrypt_stream( const std::string &, const aes_key_t &IV = def_IV );
        };
    };

    /**
     * Analog of the encryptors namespace but decrypts
     */
    namespace decryptors {
        class decryptor {
        protected:
            virtual void decrypt( const aes_key_t &block, uint8_t round ) = 0;

        public:
            virtual aes_key_ptr_t decrypt_block( const aes_key_t & ) = 0;

            virtual aes_key_ptr_t decrypt_stream( const aes_key_t & ) = 0;

            virtual aes_key_ptr_t decrypt_stream( const std::string & ) = 0;
        };

        class decrypt_ecb : public decryptor {
        protected:
            //variables
            aes_key_t m_key;

            aes_key_ptr_t m_expanded_key;

            aes_key_t m_state;

            size_t m_keyLength;

            virtual void decrypt( const aes_key_t &block, uint8_t round );

        public:
            decrypt_ecb();

            explicit decrypt_ecb( const aes_key_t &key );

            virtual ~decrypt_ecb() {}

            virtual aes_key_ptr_t decrypt_block( const aes_key_t & );

            virtual aes_key_ptr_t decrypt_stream( const aes_key_t & );

            virtual aes_key_ptr_t decrypt_stream( const std::string & );
        };

        class decrypt_cbc : public decrypt_ecb {
        public:
            explicit decrypt_cbc( const aes_key_t &key )
                    : decrypt_ecb( key ) {}

            virtual ~decrypt_cbc() {}

            virtual aes_key_ptr_t decrypt_stream( const aes_key_t &, const aes_key_t &IV = def_IV );

            virtual aes_key_ptr_t decrypt_stream( const std::string &, const aes_key_t &IV = def_IV );
        };
    };
};


#endif //CRYPTO_PROGRAMS_AES_H
